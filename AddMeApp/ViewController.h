//
//  ViewController.h
//  AddMeApp
//
//  Created by Brendan Campbell on 6/16/16.
//  Copyright (c) 2016 Wolverine Games LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;

- (IBAction)addUser:(id)sender;
- (IBAction)userTouchedView:(id)sender;


@end

