//
//  HomeScreenViewController.h
//  AddMeApp
//
//  Created by Brendan Campbell on 7/13/16.
//  Copyright © 2016 Wolverine Games LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeScreenViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuButton;

@end
