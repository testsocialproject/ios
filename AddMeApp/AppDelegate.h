//
//  AppDelegate.h
//  AddMeApp
//
//  Created by Brendan Campbell on 6/16/16.
//  Copyright (c) 2016 Wolverine Games LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

