//
//  LoginViewController.m
//  AddMeApp
//
//  Created by Brendan Campbell on 7/13/16.
//  Copyright © 2016 Wolverine Games LLC. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _username.delegate = self;
    _password.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField == _username){
        [_password becomeFirstResponder];
    }else if(textField == _password){
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    return YES;
}

- (IBAction)userTouchedScreen:(id)sender {
    [self.view endEditing:YES];
}
@end
