//
//  LoginViewController.h
//  AddMeApp
//
//  Created by Brendan Campbell on 7/13/16.
//  Copyright © 2016 Wolverine Games LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;

- (IBAction)userTouchedScreen:(id)sender;

@end
