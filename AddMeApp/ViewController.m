//
//  ViewController.m
//  AddMeApp
//
//  Created by Brendan Campbell on 6/16/16.
//  Copyright (c) 2016 Wolverine Games LLC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_username setDelegate:self];
    [_email setDelegate:self];
    [_password setDelegate:self];
    [_confirmPassword setDelegate:self];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillShow:(NSNotification *)notification{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
    if(textField != _username){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField == _username){
        [_email becomeFirstResponder];
    }else if(textField == _email){
        [_password becomeFirstResponder];
    }else if(textField == _password){
        [_confirmPassword becomeFirstResponder];
    }else if(textField == _confirmPassword){
        [self addUser:_confirmPassword];
    }
    return YES;
}

- (IBAction)addUser:(id)sender {
    NSString *usr = _username.text;
    NSString *emailAcc = _email.text;
    NSString *pass = _password.text;
    NSString *confirmPass = _confirmPassword.text;
    
    NSURL *url;
    NSMutableURLRequest *request;
    
    if(![pass isEqualToString:confirmPass]){
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Passwords must match." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:dismiss];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(usr.length > 0 && pass.length > 0){
            
        NSString *parameter = [NSString stringWithFormat:@"username=%@&email=%@&password=%@", usr, emailAcc, pass];
        NSData *parameterData = [parameter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        url = [NSURL URLWithString: @"http://www.moderntome.com/addmeapp/useradd.php"];
        request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPBody:parameterData];
        
        [request setHTTPMethod:@"POST"];
        [request addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if(connection){
            NSLog(@"Success!");
            [self performSegueWithIdentifier:@"goToLogin" sender:self];
<<<<<<< HEAD
            
=======
>>>>>>> user_login
        }else{
            NSLog(@"Failed.");
        }
       
        
    }
}

- (IBAction)userTouchedView:(id)sender {
    [self.view endEditing:YES];
}
@end
