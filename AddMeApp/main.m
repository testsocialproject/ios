//
//  main.m
//  AddMeApp
//
//  Created by Brendan Campbell on 6/16/16.
//  Copyright (c) 2016 Wolverine Games LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
